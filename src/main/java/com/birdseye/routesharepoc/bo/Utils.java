package com.birdseye.routesharepoc.bo;

import com.birdseye.routesharepoc.beans.Coordinate;

public class Utils {
	public static Coordinate stringToGeoPoint(String point){
		return new Coordinate(Double.parseDouble(point.split(",")[0]), Double.parseDouble(point.split(",")[1]));
	}
}
