package com.birdseye.routesharepoc.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.beans.Direction;
import com.birdseye.routesharepoc.beans.MatchingEdge;
import com.birdseye.routesharepoc.beans.Route;
import com.birdseye.routesharepoc.dao.RouteDAO;
import com.birdseye.routesharepoc.route.OSM;
import com.birdseye.routesharepoc.route.Singleton;

@Service("routeBO")
public class RouteBO {

	@Autowired
	RouteDAO routeDAO;

	public List<MatchingEdge> getRoute(String sourcePlaceId, String destinationPlaceId, Coordinate sourceCords, Coordinate destinationCords, int matchingPercent){
		OSM osm = new OSM();
		Direction direction = osm.getDirection(sourceCords.getLatitude(), sourceCords.getLongitude(), destinationCords.getLatitude(), 
				destinationCords.getLongitude(), false, 0);
		List<Route> routes = direction.getRoutes();
		Route route = routes.get(0);
		
		List<MatchingEdge> listMatchingEdge = getMatchingEdge(route.getEdges(), route.getPath(), route.getDistance(), matchingPercent);
		
		if(listMatchingEdge.size() < 1){
			routeDAO.addNodeList(route.getEdgeList());
			int routeId = routeDAO.addRoute(sourcePlaceId, destinationPlaceId, sourceCords, destinationCords, route.getPath(), Singleton.getInstance().getMaxNodeId());
			MatchingEdge matchingEdge = new MatchingEdge();
			matchingEdge.setAllEdgeWeight(route.getDistance());
			matchingEdge.setPath(route.getPath());
			matchingEdge.setNew(true);
			matchingEdge.setRouteId(routeId);
			listMatchingEdge.add(matchingEdge);
			
		}
		return listMatchingEdge;
	}

	private List<MatchingEdge> getMatchingEdge(List<Integer> edges, String actualPath, double routeLength,  int matchingPercent){
		return routeDAO.getMatchingEdge(edges, actualPath, routeLength, matchingPercent);
	}
	
	
}
