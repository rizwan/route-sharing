package com.birdseye.routesharepoc.bo;

import java.util.List;

import com.birdseye.routesharepoc.beans.ParentBoxes;
import com.birdseye.routesharepoc.beans.TravelTime;
import com.birdseye.routesharepoc.dao.TravelTimeDAO;
import com.graphhopper.routing.Path;

public class TravelTimeBO {
	public List<TravelTime> GetTravelTime(List<Path> path){
		getParentBoxForPath(path.get(0));
		return null;
	}
	
	private List<ParentBoxes> getParentBoxForPath(Path path){
		TravelTimeDAO tteDAO = new TravelTimeDAO();
		return tteDAO.getParentboxes(path);
	}
}
