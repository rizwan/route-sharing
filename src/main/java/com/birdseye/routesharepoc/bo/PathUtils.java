package com.birdseye.routesharepoc.bo;

import com.graphhopper.routing.Path;
import com.graphhopper.util.PointList;

public class PathUtils {
	public static String getPathString(Path path){
		PointList list = path.calcPoints();
		StringBuilder stringBuilder = new StringBuilder();
		for(int i = 0; i< list.getSize(); i++){
			stringBuilder.append(list.getLongitude(i) + "$" + list.getLatitude(i) + "|");
		}
		return stringBuilder.substring(0, stringBuilder.length() - 1);
	}
}
