package com.birdseye.routesharepoc.route;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.beans.Direction;
import com.birdseye.routesharepoc.beans.MatchingEdge;
import com.birdseye.routesharepoc.beans.Response;
import com.birdseye.routesharepoc.bo.Utils;
import com.birdseye.routesharepoc.publictransport.beans.SharePTResponse;
import com.birdseye.routesharepoc.publictransport.service.PlannerIterator;
import com.birdseye.routesharepoc.bo.RouteBO;

@Controller
public class DirectionController {

	@Autowired
	RouteBO routeBO;

	@Autowired
	PlannerIterator plannerIterator;

	@RequestMapping(value = "distance", method = {RequestMethod.GET})
	public List<Response>  greeting(@RequestParam(value="points", required=false, defaultValue="World") String data) {
		OSM osm = new OSM();
		List<Response> response  = osm.GetDistance(data);
		return response;
		//return new List<Response>()
	}

	@RequestMapping(value= "direction", method={RequestMethod.GET})
	@ResponseBody
	public Direction getDirectionWithAlternatives(
			@RequestParam(value="start", required=true) String start, 
			@RequestParam(value="end", required=true) String end,
			@RequestParam(value="alternatives", required = false) Integer number,
			@RequestParam(value="via", required=false) String points){
		OSM osm = new OSM();
		boolean alternateRouteRequest = false;
		Coordinate from = Utils.stringToGeoPoint(start);
		int noOfRoute = 0;
		if(number != null && number > 0){
			alternateRouteRequest = true;
			noOfRoute = number;
		}
		osm.setVia(points);

		Coordinate to = Utils.stringToGeoPoint(end);
		Direction dirs = osm.getDirection(from.getLatitude(), from.getLongitude(), to.getLatitude(), to.getLongitude(), alternateRouteRequest, noOfRoute);
		return dirs;
	}


	@RequestMapping(value= "sharedroute", method={RequestMethod.GET})
	@ResponseBody
	public List<MatchingEdge> getShareRoute(
			@RequestParam(value="start", required=true) String start, 
			@RequestParam(value="end", required=true) String end,
			@RequestParam(value="alternatives", required = false) Integer number,
			@RequestParam(value="via", required=false) String points,
			@RequestParam(value="matchpercent", required=false) Integer matchPercent){

		Coordinate from = Utils.stringToGeoPoint(start);
		Coordinate to = Utils.stringToGeoPoint(end);
		//Direction dirs = osm.getDirection(from.getLatitude(), from.getLongitude(), to.getLatitude(), to.getLongitude(), alternateRouteRequest, noOfRoute);

		return routeBO.getRoute("", "", from, to, matchPercent);
	}

	@RequestMapping(value= "sharedpublictransport", method={RequestMethod.GET})
	@ResponseBody
	public SharePTResponse getSharedPT(
			@RequestParam(value="start", required=true) String start, 
			@RequestParam(value="end", required=true) String end,
			@RequestParam(value="alternatives", required = false) Integer number,
			@RequestParam(value="via", required=false) String points,
			@RequestParam(value="matchpercent", required=false) Integer matchPercent){

		Coordinate from = Utils.stringToGeoPoint(start);
		Coordinate to = Utils.stringToGeoPoint(end);
		//Direction dirs = osm.getDirection(from.getLatitude(), from.getLongitude(), to.getLatitude(), to.getLongitude(), alternateRouteRequest, noOfRoute);
		boolean routeExist = plannerIterator.doTheBusiness(from, to);
		if(!routeExist){
			plannerIterator.addRoute(from, to);
		}
		return plannerIterator.sharePTResponse;
		//return routeBO.getRoute("", "", from, to, matchPercent);
		//return null;
	}

	@RequestMapping("mapping")
	public String getMapPage(Model model){
		model.addAttribute("name","rizwan");
		return "map";
	}
}
