package com.birdseye.routesharepoc.route;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;

import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;

public class Singleton {
	protected Singleton(){}
	
	private int maxNodeId = 0;
	private static Singleton instance = null;
	private GraphHopper graphHopper = null;
	public static Singleton getInstance(){
		if(instance == null){
			instance = new Singleton();
		}
		return instance;
	}
	
	public GraphHopper getGraphHopper(){
		if(null == graphHopper){
			Properties properties = null;
			InputStream input;
			try {
				
				properties = new Properties();
				properties.load(this.getClass().getClassLoader().getResourceAsStream("application.properties"));
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			graphHopper = new GraphHopper().forServer();
			graphHopper.setInMemory();//(true,false);
			graphHopper.setOSMFile(properties.getProperty("osm_data_dir")+"/mumbai.osm");
			
			// where to store graphhopper files?
			graphHopper.setGraphHopperLocation(properties.getProperty("graphhopper_workspace"));
			graphHopper.setEncodingManager(new EncodingManager(EncodingManager.CAR));
			graphHopper.setCHWeighting("shortest");

			// now this can take minutes if it imports or a few seconds for loading
			// of course this is dependent on the area you import
			graphHopper.importOrLoad();
		}
		return graphHopper;
	}

	public int getMaxNodeId() {
		return maxNodeId;
	}

	public void setMaxNodeId(int maxNodeId) {
		this.maxNodeId = maxNodeId;
	}
	
	
}
