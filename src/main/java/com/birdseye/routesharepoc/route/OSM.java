package com.birdseye.routesharepoc.route;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.springframework.util.StringUtils;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.beans.Direction;
import com.birdseye.routesharepoc.beans.Response;
import com.birdseye.routesharepoc.beans.Route;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import com.graphhopper.util.InstructionList;
import com.graphhopper.util.PointList;
import com.graphhopper.util.shapes.GHPoint;


public class OSM {

	/**
	 * @param args
	 * @throws ParseException 
	 * @throws Throwable 
	 */
	private String via;
	public OSM setVia(String via){
		this.via = via;
		return this;
	}
	
	private static org.apache.log4j.Logger log = org.apache.log4j.LogManager.getLogger(OSM.class.getName());
	public List<Response> GetDistance(String data) {
		// TODO Auto-generated method stub
		List<Response> lstResponse = new ArrayList<Response>();
		GraphHopper hopper = Singleton.getInstance().getGraphHopper();
		log.debug("data " + data);
		String[] row = data.split("\\|");

		GHRequest req = null;
		//JSONArray jsonArr = null;
		//String data = "[{source:{lat:19.2390,lng:72.29},destination:{lat:19.2390,lng:72.29}},{source:{lat:19.2390,lng:72.29},destination:{lat:19.2390,lng:72.29}}]";		StringBuffer sbOutput = new StringBuffer();
		for(int j = 0; j< row.length; j++) {
			log.debug("js is " + j);
			System.out.print("New Request \nStart - " + System.currentTimeMillis() + "\n");
			Response response = new Response();
			//JSONObject jsonObj = (JSONObject)jsonArr.get(j);

			String source = row[j].split("-")[0];
			String destination = row[j].split("-")[1];
			double latFrom = Double.parseDouble(source.split(",")[0]);
			double lngFrom = Double.parseDouble(source.split(",")[1]);
			double latTo = Double.parseDouble(destination.split(",")[0]);
			double lngTo = Double.parseDouble(destination.split(",")[1]);

			response.setFrom(new Coordinate(latFrom, lngFrom));
			response.setTo(new Coordinate(latTo, lngTo));

			req = new GHRequest(latFrom, lngFrom, latTo, lngTo).setVehicle(EncodingManager.FOOT);
			GHResponse rsp = hopper.route(req);
			if(rsp.hasErrors()) {
				// handle them!

				System.out.println("There is error");
				continue;
				//   return;
			}

			// route was found? e.g. if disconnected areas (like island) 
			// no route can ever be found
			if(!rsp.isFound()) {
				// handle properly
				System.out.println("route not found");
				//return;
				continue;
			}

			// points, distance in meters and time in millis of the full path

			//PointList pointList = rsp.getPoints();
			double distance = rsp.getDistance();
			//long millis = rsp.getTime();
			//StringBuffer strBuffer = new StringBuffer();

			//for(int i = 0; i< pointList.getSize(); i++){
			//	strBuffer.append(pointList.getLatitude(i) + "," + pointList.getLongitude(i) + "\n");
			//}

			response.setDistanceKilometer(distance);
			//response.setTime(millis);
			//System.out.println(strBuffer.toString());
			lstResponse.add(response);

		}

		return lstResponse;



		// first check for errors


		// get the turn instructions for the path
		/*
		InstructionList il = rsp.getInstructions();
		Translation tr = trMap.getWithFallBack(Locale.US);
		List<String> iList = il.createDescription(tr);
		 */
		// or get the result as gpx entries:
		//List<GPXEntry> list = il.createGPXList();
		//return new ArrayList<Response>();
	}


	public List<Direction> getDirection(String data){
		log.debug("data " + data);
		String[] row = data.split("\\|");
		List<Direction> lstDirection = new ArrayList<Direction>();
		Direction direction = null;
		for(int j = 0; j< row.length; j++) {
			String source = row[j].split("-")[0];
			String destination = row[j].split("-")[1];
			double latFrom = Double.parseDouble(source.split(",")[0]);
			double lngFrom = Double.parseDouble(source.split(",")[1]);
			double latTo = Double.parseDouble(destination.split(",")[0]);
			double lngTo = Double.parseDouble(destination.split(",")[1]);
			direction = new Direction();
			direction.setFrom(new Coordinate(latFrom, lngFrom));
			direction.setTo(new Coordinate(latTo, lngTo));
			getRoute(direction, false, 0);
			lstDirection.add(direction);
		}	
		return lstDirection;
	}

	public List<Direction> getDirection(String source, String destinations){
		String[] row = destinations.split("\\|");
		List<Direction> lstDirection = new ArrayList<Direction>();
		Direction direction = null;
		double latFrom = Double.parseDouble(source.split(",")[0]);
		double lngFrom = Double.parseDouble(source.split(",")[1]);
		for(int j = 0; j< row.length; j++) {
			double latTo = Double.parseDouble(row[j].split(",")[0]);
			double lngTo = Double.parseDouble(row[j].split(",")[1]);
			direction = new Direction();
			direction.setFrom(new Coordinate(latFrom, lngFrom));
			direction.setTo(new Coordinate(latTo, lngTo));
			getRoute(direction, false, 0);
			lstDirection.add(direction);
		}	
		return lstDirection;
	}

	public Direction getDirection(double fromLat, double fromLng, double toLat, double toLng, boolean alternatives, int numberOfRoutes){
		Direction direction = new Direction();
		direction.setFrom(new Coordinate(fromLat, fromLng));
		direction.setTo(new Coordinate(toLat, toLng));
		getRoute(direction, alternatives, numberOfRoutes);
		return direction;
	}

	private void getRoute(Direction direction, boolean alternates, int numberOfRoutes){

		GraphHopper hopper = Singleton.getInstance().getGraphHopper();


		List<Route> lstRoute = new ArrayList<Route>();
		GHRequest req = null;
		Route route = null;
		List<GHResponse> lstRes = null;
		lstRoute = new ArrayList<Route>();
		System.out.print("New Request \nStart - " + System.currentTimeMillis() + "\n");

		req = new GHRequest(direction.getFrom().getLatitude(), direction.getFrom().getLongitude(), 
				direction.getTo().getLatitude(), direction.getTo().getLongitude()).
				setVehicle(EncodingManager.CAR).setAlternates(alternates, numberOfRoutes).setWeighting("shortest");
		GHResponse rsp = null;
		if(!alternates && StringUtils.isEmpty(via)){
			rsp = hopper.route(req);
			if(rsp.hasErrors()){
				return ;
			}
			lstRes = new ArrayList<GHResponse>();
			lstRes.add(rsp);
		}
		else if(!StringUtils.isEmpty(via)){
			String[] points = via.split("\\|");
			req = new GHRequest().setVehicle(EncodingManager.CAR).setWeighting("shortest").
					addPoint(new GHPoint(direction.getFrom().getLatitude(), direction.getFrom().getLongitude()));
			GHPoint ghPoint = null;
			for(int i = 0; i < points.length; i++){
				ghPoint = new GHPoint(Double.parseDouble(points[i].split(",")[0]), Double.parseDouble(points[i].split(",")[1])); 
				req.addPoint(ghPoint);
			}
			req.addPoint(new GHPoint(direction.getTo().getLatitude(), direction.getTo().getLongitude()));
			rsp = hopper.route(req);
			lstRes = new ArrayList<GHResponse>();
			lstRes.add(rsp);
		
		}else{
			lstRes = hopper.alternateRoute(req);
		}

		// points, distance in meters and time in millis of the full path
		for(GHResponse response : lstRes){
			route = new Route();
			if(response.hasErrors()) {
				System.out.println(rsp.getDebugInfo());
				return;
			}

			// route was found? e.g. if disconnected areas (like island) 
			// no route can ever be found
			if(!response.isFound()) {
				// handle properly
				System.out.println("Route not found");
				//return;
				return;
			}
			PointList pointList = response.getPoints();
			if(alternates){

				req = new GHRequest().
						setVehicle(EncodingManager.CAR).setWeighting("shortest");
				req.addPoint(new GHPoint(direction.getFrom().getLatitude(), direction.getFrom().getLongitude()));
				for(int i = 1; i< pointList.getSize() -1; i++){
					req.addPoint(new GHPoint(pointList.getLatitude(i), pointList.getLongitude(i)));
				}	
				req.addPoint(new GHPoint(direction.getTo().getLatitude(), direction.getTo().getLongitude()));
				response = hopper.route(req);

				pointList = response.getPoints();
			}
			
			StringBuffer strBuffer = new StringBuffer();
			for(int i = 0; i< pointList.getSize(); i++){
				strBuffer.append(pointList.getLongitude(i) + "$" + pointList.getLatitude(i) + "|");
			}
			InstructionList instructions = response.getInstructions();
			double distance = response.getDistance();
			double time = response.getMillis();

			List<String> arrInstruction = new ArrayList<String>();
			for(int i = 0; i< instructions.getSize(); i++){
				arrInstruction.add(instructions.get(i).getName());
			}	

			route.setInstructions(arrInstruction);
			route.setPath(strBuffer.substring(0, strBuffer.length() -1));
			route.setDistance((Math.round(distance)*100)/100);
			route.setTime((int)time);
			
			route.setEdgeList(response.getEdgeList());
			route.setEdges(response.getEdges());
			lstRoute.add(route);
			
			direction.setRoutes(lstRoute);
		}
	}
}
/*
	public List<Direction> getViaDirection(Coordinate start, Coordinate end, List<Coordinate> via){
		List<Direction> lstDirection = new ArrayList<Direction>();
		Direction direction = null;
		GraphHopper hopper = Singleton.getInstance().getGraphHopper();
		List<Route> lstRoute = new ArrayList<Route>();
		GHRequest req = null;
		Route route = null;
		//JSONArray jsonArr = null;
		//String data = "[{source:{lat:19.2390,lng:72.29},destination:{lat:19.2390,lng:72.29}},{source:{lat:19.2390,lng:72.29},destination:{lat:19.2390,lng:72.29}}]";		StringBuffer sbOutput = new StringBuffer();

		//log.debug("js is " + j);
		lstRoute = new ArrayList<Route>();
		route = new Route();
		direction = new Direction();
		System.out.print("New Request \nStart - " + System.currentTimeMillis() + "\n");
		//Direction response = new Direction();
		//JSONObject jsonObj = (JSONObject)jsonArr.get(j);
		direction.setFrom(start);
		direction.setTo(end);
		req = new GHRequest();
		req.addPoint(new GHPoint(start.getLatitude(), start.getLongitude()));
		addVia(req, via);
		req.addPoint(new GHPoint(end.getLatitude(), end.getLongitude()));
		req.setVehicle(EncodingManager.CAR);
		GHResponse rsp = hopper.route(req);
		if(rsp.hasErrors()) {
			// handle them!

			System.out.println("There is error");

			//   return;
		}

		// route was found? e.g. if disconnected areas (like island) 
		// no route can ever be found
		if(!rsp.isFound()) {
			// handle properly
			System.out.println("route not found");
			//return;

		}

		// points, distance in meters and time in millis of the full path

		PointList pointList = rsp.getPoints();
		double distance = rsp.getDistance();
		//long millis = rsp.getTime();

		StringBuffer strBuffer = new StringBuffer();
		for(int i = 0; i< pointList.getSize(); i++){
			strBuffer.append(pointList.getLongitude(i) + "$" + pointList.getLatitude(i) + "|");
		}			
		route.setPath(strBuffer.substring(0, strBuffer.length() -1));
		route.setDistance(String.valueOf(distance));
		lstRoute.add(route);
		direction.setRoutes(lstRoute);
		lstDirection.add(direction);	
		direction.setRoutes(lstRoute);
		return lstDirection;
	}

	private void addVia(GHRequest req, List<Coordinate> lstCordinate){
		GHPoint ghPoint = null;
		for(Coordinate cordinate : lstCordinate){
			ghPoint = new GHPoint(cordinate.getLatitude(), cordinate.getLongitude());
			req.addPoint(ghPoint);
		}
	}*/
