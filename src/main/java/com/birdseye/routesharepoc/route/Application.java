package com.birdseye.routesharepoc.route;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@ComponentScan (basePackages={"com.birdseye.routesharepoc"})
@Configuration
@EnableAutoConfiguration
public class Application {

    public static void main(String[] args) {
    	Singleton.getInstance().getGraphHopper();
        SpringApplication.run(Application.class, args);
    }
}
