package com.birdseye.routesharepoc.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.birdseye.routesharepoc.beans.ParentBoxes;
import com.birdseye.routesharepoc.beans.Queries;
import com.birdseye.routesharepoc.bo.PathUtils;
import com.graphhopper.routing.Path;

public class TravelTimeDAO {
	public List<ParentBoxes> getParentboxes(Path path){
		Session session = HibernateSession.getSessionFactory().getCurrentSession();
		Query query = session.createQuery(Queries.GET_PARENTBOX_FOR_PATH);
		query.setParameter(1, PathUtils.getPathString(path));
		List<ParentBoxes> parentboxes = query.list();
		return parentboxes;
	}
}
