package com.birdseye.routesharepoc.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.beans.MatchingEdge;
import com.birdseye.routesharepoc.route.Singleton;
import com.graphhopper.util.EdgeIteratorState;
import com.graphhopper.util.PointList;

@Repository("routeDAO")
public class RouteDAO {

	//@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate();
		this.jdbcTemplate.setDataSource(dataSource);
	}

	public int addRoute(final String sourcePlaceId, final String destinationPlaceId, final Coordinate sourceCoordinate, final Coordinate destinationCordinate, final String path, int edgeId){
		final String routeQuery = "insert into route (source_place_id, destination_place_id, source_coordinates, destination_coordinates, path) values (?,?,?,?,?)";
		String routeNodeMappingQuery = "insert into route_edge_mapping(route_id, edge_id) value (?,?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(
				new PreparedStatementCreator() {
					public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
						PreparedStatement ps =
								connection.prepareStatement(routeQuery, new String[] {"route_id"});
						ps.setString(1, sourcePlaceId);
						ps.setString(2, destinationPlaceId);
						ps.setString(3, sourceCoordinate.toString());
						ps.setString(4, destinationCordinate.toString());
						ps.setString(5, path);
						return ps;
					}
				},
				keyHolder);
		int routeId = keyHolder.getKey().intValue();

		//add in route_edge_mapping table
		if(edgeId > 0)
			jdbcTemplate.update(routeNodeMappingQuery, routeId, edgeId);
		return routeId;
	}


	public void addNodeList(final List<EdgeIteratorState> nodeList){
		String query = "insert into edge (edge_id, edge_val, edge_weight, edge_points) value (?,?,?,?)";

		final int maxEdgeId = this.getMaxEdgeId();
		jdbcTemplate.batchUpdate(query, new BatchPreparedStatementSetter() {
			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				Integer val = nodeList.get(i).getEdge();
				double distance = nodeList.get(i).getDistance();
				ps.setInt(1,  maxEdgeId);
				ps.setInt(2, val);
				ps.setDouble(3, distance);
				StringBuilder sbPointList = new StringBuilder();
				PointList pointList =  nodeList.get(i).fetchWayGeometry(2);
				for(int j = 0; j < pointList.getSize(); j++){
					sbPointList.append(pointList.getLongitude(j) + "$" + pointList.getLatitude(j) + "|");
				}
				ps.setString(4, sbPointList.substring(0, sbPointList.length()-1).toString());
			}

			@Override
			public int getBatchSize() {
				// TODO Auto-generated method stub
				return nodeList.size();
			}
		});
	}

	//get the matching node from data base and it's weight percentage
	public List<MatchingEdge> getMatchingEdge(List<Integer> nodeList, String actualPath, double routeLength, int matchingPercent){
		String strNodeList = StringUtils.collectionToCommaDelimitedString(nodeList);
		String query = "select d.route_id, a.edge_id, matching_edge_count, count(b.edge_id) all_edge, matching_edge_weight, "
				+ "round((matching_edge_weight/" + routeLength + "*100),0) 'percent_match', d.path, "
				+ " matching_point"
				+ " from ("
				+ "select edge_id, count(*) matching_edge_count, sum(edge_weight) matching_edge_weight, "
				+ "group_concat(edge_points separator '|') matching_point from edge "
				+ "where find_in_set(edge_val,'" + strNodeList + "') "
				+ "group by edge_id) a join edge b on a.edge_id = b.edge_id "
				+ "join route_edge_mapping c on b.edge_id = c.edge_id join route d on d.route_id = c.route_id "
				+ "group by b.edge_id having ((matching_edge_weight)/" + routeLength + "*100) >= " + matchingPercent;
		List<MatchingEdge> listMatchingNode = new ArrayList<MatchingEdge>();
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(query);
		if (rows == null)
			return listMatchingNode;

		for(Map<String, Object> row : rows){
			MatchingEdge matchingNode = new MatchingEdge();
			matchingNode.setEdgeId(Integer.parseInt(row.get("edge_id").toString()));
			matchingNode.setAllEdgeCount(Integer.parseInt(row.get("all_edge").toString()));
			matchingNode.setAllEdgeWeight(routeLength);
			matchingNode.setMatchingEdgeCount(Integer.parseInt(row.get("matching_edge_count").toString()));
			matchingNode.setMatchingEdgeWeight(Double.parseDouble(row.get("matching_edge_weight").toString()));
			matchingNode.setMatchingPercent((int)Double.parseDouble(row.get("percent_match").toString()));
			matchingNode.setRouteId((int)row.get("route_id"));
			String matchPoint = row.get("matching_point").toString();
			matchingNode.setMatchingPath(matchPoint.substring(0, matchPoint.length()-1));
			matchingNode.setPath(actualPath);
			listMatchingNode.add(matchingNode);
		}
		return listMatchingNode;
	}


	private int getMaxEdgeId(){
		if(Singleton.getInstance().getMaxNodeId() == -1){
			String query = "select max(edge_id) from edge order by edge_id  desc limit 1";
			Object data = this.jdbcTemplate.queryForObject(query, Object.class);
			int id = 0;
			if(null != data)
				id = (Integer)data + 1;
			else
				id = 0;
			Singleton.getInstance().setMaxNodeId(id);
			return id;
		}else{
			Singleton.getInstance().setMaxNodeId(Singleton.getInstance().getMaxNodeId() + 1);
			return Singleton.getInstance().getMaxNodeId();
		}

	}
}
