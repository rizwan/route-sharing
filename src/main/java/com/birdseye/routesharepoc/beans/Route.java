package com.birdseye.routesharepoc.beans;

import java.util.List;

import com.graphhopper.util.EdgeIteratorState;

public class Route {
	double distance;
	String path;
	int time;
	private List<String> instructions;
	transient private List<Integer> edges;
	transient private List<EdgeIteratorState> edgeList;
	
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	public List<String> getInstructions() {
		return instructions;
	}
	public void setInstructions(List<String> instructions) {
		this.instructions = instructions;
	}
	public List<Integer> getEdges() {
		return edges;
	}
	public void setEdges(List<Integer> edges) {
		this.edges = edges;
	}
	public List<EdgeIteratorState> getEdgeList() {
		return edgeList;
	}
	public void setEdgeList(List<EdgeIteratorState> edgeList) {
		this.edgeList = edgeList;
	}
	
	
}
