package com.birdseye.routesharepoc.beans;

import java.util.List;

public class Direction{
	private Coordinate from = null;
	private Coordinate to = null;
	private List<Route> routes = null;
	
	public Coordinate getFrom() {
		return from;
	}
	public void setFrom(Coordinate from) {
		this.from = from;
	}
	public Coordinate getTo() {
		return to;
	}
	public void setTo(Coordinate to) {
		this.to = to;
	}
	public List<Route> getRoutes() {
		return routes;
	}
	public void setRoutes(List<Route> routes) {
		this.routes = routes;
	}
}
