package com.birdseye.routesharepoc.beans;


public class Response {
	private Coordinate from = null;
	private Coordinate to = null;

	private String distanceKilometer;

	public Coordinate getFrom() {
		return from;
	}
	public void setFrom(Coordinate from) {
		this.from = from;
	}
	public Coordinate getTo() {
		return to;
	}
	public void setTo(Coordinate to) {
		this.to = to;
	}

	public String getDistanceKilometer() {
		return distanceKilometer;
	}

	public void setDistanceKilometer(double distanceKilometer) {
		this.distanceKilometer = Math.round((distanceKilometer/1000)*100.0)/100.0 + "km";
	}
}
