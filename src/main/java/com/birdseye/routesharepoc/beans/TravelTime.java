package com.birdseye.routesharepoc.beans;

import com.graphhopper.routing.Path;

/**
 * @author Admin
 *
 */
public class TravelTime {
	Path path;
	double distance;
	int time;
	
	public Path getPath() {
		return path;
	}
	public void setPath(Path path) {
		this.path = path;
	}
	public double getDistance() {
		return distance;
	}
	public void setDistance(double distance) {
		this.distance = distance;
	}
	public int getTime() {
		return time;
	}
	public void setTime(int time) {
		this.time = time;
	}
	
}
