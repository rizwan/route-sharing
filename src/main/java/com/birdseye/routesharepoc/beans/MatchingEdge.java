package com.birdseye.routesharepoc.beans;

public class MatchingEdge {
	private int edgeId;
	private int allEdgeCount;
	private int matchingEdgeCount;
	private double allEdgeWeight;
	private double matchingEdgeWeight;
	private int matchingPercent;
	private String matchingPath;
	private String path;
	private int routeId;
	private boolean isNew = false;
	
	public int getEdgeId() {
		return edgeId;
	}
	public void setEdgeId(int edgeId) {
		this.edgeId = edgeId;
	}
	public int getAllEdgeCount() {
		return allEdgeCount;
	}
	public void setAllEdgeCount(int allEdgeCount) {
		this.allEdgeCount = allEdgeCount;
	}
	public int getMatchingEdgeCount() {
		return matchingEdgeCount;
	}
	public void setMatchingEdgeCount(int matchingEdgeCount) {
		this.matchingEdgeCount = matchingEdgeCount;
	}
	public double getAllEdgeWeight() {
		return allEdgeWeight;
	}
	public void setAllEdgeWeight(double allEdgeWeight) {
		this.allEdgeWeight = allEdgeWeight;
	}
	public double getMatchingEdgeWeight() {
		return matchingEdgeWeight;
	}
	public void setMatchingEdgeWeight(double matchingEdgeWeight) {
		this.matchingEdgeWeight = matchingEdgeWeight;
	}
	public int getMatchingPercent() {
		return matchingPercent;
	}
	public void setMatchingPercent(int matchingPercent) {
		this.matchingPercent = matchingPercent;
	}
	public String getMatchingPath() {
		return matchingPath;
	}
	public void setMatchingPath(String matchingPath) {
		this.matchingPath = matchingPath;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public int getRouteId() {
		return routeId;
	}
	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}
	public boolean isNew() {
		return isNew;
	}
	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}
	
	
	
}
