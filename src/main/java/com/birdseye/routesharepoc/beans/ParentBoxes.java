package com.birdseye.routesharepoc.beans;

public class ParentBoxes {
	private int id;
	private double heading;
	private int roadAreaId;
	private String parentbox;
	private String direction;
	private int roadId;
	private double roadAreaLength;
	private String roadName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public double getHeading() {
		return heading;
	}
	public void setHeading(double heading) {
		this.heading = heading;
	}
	public int getRoadAreaId() {
		return roadAreaId;
	}
	public void setRoadAreaId(int roadAreaId) {
		this.roadAreaId = roadAreaId;
	}
	public String getParentbox() {
		return parentbox;
	}
	public void setParentbox(String parentbox) {
		this.parentbox = parentbox;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public int getRoadId() {
		return roadId;
	}
	public void setRoadId(int roadId) {
		this.roadId = roadId;
	}
	public double getRoadAreaLength() {
		return roadAreaLength;
	}
	public void setRoadAreaLength(double roadAreaLength) {
		this.roadAreaLength = roadAreaLength;
	}
	public String getRoadName() {
		return roadName;
	}
	public void setRoadName(String roadName) {
		this.roadName = roadName;
	}
}
