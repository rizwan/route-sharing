package com.birdseye.routesharepoc.publictransport.beans;

import java.util.List;

/**
 * Created by Pushan on 21/3/15.
 */
public class Plans  {

	private String duration;
	private String walkDuration;
	private String starttimestamp;
	private String endtimestamp;
	private String localTransport;
	private List<Leg> legs;

	//private NPlanRequest planRequest;

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getWalkDuration() {
		return walkDuration;
	}

	public void setWalkDuration(String walkDuration) {
		this.walkDuration = walkDuration;
	}

	public String getStarttimestamp() {
		return starttimestamp;
	}

	public void setStarttimestamp(String starttimestamp) {
		this.starttimestamp = starttimestamp;
	}

	public String getEndtimestamp() {
		return endtimestamp;
	}

	public void setEndtimestamp(String endtimestamp) {
		this.endtimestamp = endtimestamp;
	}

	public List<Leg> getLegs() {
		return legs;
	}

	public void setLegs(List<Leg> legs) {
		this.legs = legs;
	}

	/*public NPlanRequest getPlanRequest() {
		return planRequest;
	}

	public void setPlanRequest(NPlanRequest planRequest) {
		this.planRequest = planRequest;
	}
*/
	public Plans[] newArray(int size) {
		return new Plans[size];
	}

public String getLocalTransport() {
	return localTransport;
}

public void setLocalTransport(String localTransport) {
	this.localTransport = localTransport;
}
}
