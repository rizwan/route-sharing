package com.birdseye.routesharepoc.publictransport.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("org.jsonschema2pojo")
@JsonPropertyOrder({
    "arrivalTime",
    "cityId",
    "departureTime",
    "destinationPlace",
    "destinationPlaceId",
    "deviceId",
    "localTransport",
    "mode",
    "points",
    "preference",
    "resultPerLocalTransport",
    "sourcePlace",
    "sourcePlaceId",
    "userId"
})

public class Request {

    @JsonProperty("arrivalTime")
    private Object arrivalTime;
    @JsonProperty("cityId")
    private String cityId;
    @JsonProperty("departureTime")
    private String departureTime;
    @JsonProperty("destinationPlace")
    private String destinationPlace;
    @JsonProperty("destinationPlaceId")
    private Object destinationPlaceId;
    @JsonProperty("deviceId")
    private String deviceId;
    @JsonProperty("localTransport")
    private List<String> localTransport = new ArrayList<String>();
    @JsonProperty("mode")
    private List<String> mode = new ArrayList<String>();
    @JsonProperty("points")
    private List<List<Double>> points = new ArrayList<List<Double>>();
    @JsonProperty("preference")
    private String preference;
    @JsonProperty("resultPerLocalTransport")
    private String resultPerLocalTransport;
    @JsonProperty("sourcePlace")
    private String sourcePlace;
    @JsonProperty("sourcePlaceId")
    private Object sourcePlaceId;
    @JsonProperty("userId")
    private String userId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 
     * @return
     *     The arrivalTime
     */
    @JsonProperty("arrivalTime")
    public Object getArrivalTime() {
        return arrivalTime;
    }

    /**
     * 
     * @param arrivalTime
     *     The arrivalTime
     */
    @JsonProperty("arrivalTime")
    public void setArrivalTime(Object arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    /**
     * 
     * @return
     *     The cityId
     */
    @JsonProperty("cityId")
    public String getCityId() {
        return cityId;
    }

    /**
     * 
     * @param cityId
     *     The cityId
     */
    @JsonProperty("cityId")
    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    /**
     * 
     * @return
     *     The departureTime
     */
    @JsonProperty("departureTime")
    public String getDepartureTime() {
        return departureTime;
    }

    /**
     * 
     * @param departureTime
     *     The departureTime
     */
    @JsonProperty("departureTime")
    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    /**
     * 
     * @return
     *     The destinationPlace
     */
    @JsonProperty("destinationPlace")
    public String getDestinationPlace() {
        return destinationPlace;
    }

    /**
     * 
     * @param destinationPlace
     *     The destinationPlace
     */
    @JsonProperty("destinationPlace")
    public void setDestinationPlace(String destinationPlace) {
        this.destinationPlace = destinationPlace;
    }

    /**
     * 
     * @return
     *     The destinationPlaceId
     */
    @JsonProperty("destinationPlaceId")
    public Object getDestinationPlaceId() {
        return destinationPlaceId;
    }

    /**
     * 
     * @param destinationPlaceId
     *     The destinationPlaceId
     */
    @JsonProperty("destinationPlaceId")
    public void setDestinationPlaceId(Object destinationPlaceId) {
        this.destinationPlaceId = destinationPlaceId;
    }

    /**
     * 
     * @return
     *     The deviceId
     */
    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * 
     * @param deviceId
     *     The deviceId
     */
    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 
     * @return
     *     The localTransport
     */
    @JsonProperty("localTransport")
    public List<String> getLocalTransport() {
        return localTransport;
    }

    /**
     * 
     * @param localTransport
     *     The localTransport
     */
    @JsonProperty("localTransport")
    public void setLocalTransport(List<String> localTransport) {
        this.localTransport = localTransport;
    }

    /**
     * 
     * @return
     *     The mode
     */
    @JsonProperty("mode")
    public List<String> getMode() {
        return mode;
    }

    /**
     * 
     * @param mode
     *     The mode
     */
    @JsonProperty("mode")
    public void setMode(List<String> mode) {
        this.mode = mode;
    }

    /**
     * 
     * @return
     *     The points
     */
    @JsonProperty("points")
    public List<List<Double>> getPoints() {
        return points;
    }

    /**
     * 
     * @param points
     *     The points
     */
    @JsonProperty("points")
    public void setPoints(List<List<Double>> points) {
        this.points = points;
    }

    /**
     * 
     * @return
     *     The preference
     */
    @JsonProperty("preference")
    public String getPreference() {
        return preference;
    }

    /**
     * 
     * @param preference
     *     The preference
     */
    @JsonProperty("preference")
    public void setPreference(String preference) {
        this.preference = preference;
    }

    /**
     * 
     * @return
     *     The resultPerLocalTransport
     */
    @JsonProperty("resultPerLocalTransport")
    public String getResultPerLocalTransport() {
        return resultPerLocalTransport;
    }

    /**
     * 
     * @param resultPerLocalTransport
     *     The resultPerLocalTransport
     */
    @JsonProperty("resultPerLocalTransport")
    public void setResultPerLocalTransport(String resultPerLocalTransport) {
        this.resultPerLocalTransport = resultPerLocalTransport;
    }

    /**
     * 
     * @return
     *     The sourcePlace
     */
    @JsonProperty("sourcePlace")
    public String getSourcePlace() {
        return sourcePlace;
    }

    /**
     * 
     * @param sourcePlace
     *     The sourcePlace
     */
    @JsonProperty("sourcePlace")
    public void setSourcePlace(String sourcePlace) {
        this.sourcePlace = sourcePlace;
    }

    /**
     * 
     * @return
     *     The sourcePlaceId
     */
    @JsonProperty("sourcePlaceId")
    public Object getSourcePlaceId() {
        return sourcePlaceId;
    }

    /**
     * 
     * @param sourcePlaceId
     *     The sourcePlaceId
     */
    @JsonProperty("sourcePlaceId")
    public void setSourcePlaceId(Object sourcePlaceId) {
        this.sourcePlaceId = sourcePlaceId;
    }

    /**
     * 
     * @return
     *     The userId
     */
    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId
     *     The userId
     */
    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
