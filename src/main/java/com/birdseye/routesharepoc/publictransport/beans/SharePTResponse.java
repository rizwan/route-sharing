package com.birdseye.routesharepoc.publictransport.beans;

import java.util.ArrayList;
import java.util.List;

public class SharePTResponse {
	private int matchPercent;
	private List<AutoCabResponse> autoCab = new ArrayList<AutoCabResponse>();
	private List<TrainResponse> trains= new ArrayList<TrainResponse>();
	private List<BusResponse> bus= new ArrayList<BusResponse>();
	private NPlanResponse nplanResponse;
	
	public List<AutoCabResponse> getAutoCab() {
		return autoCab;
	}
	public void setAutoCab(List<AutoCabResponse> autoCab) {
		this.autoCab = autoCab;
	}
	public List<TrainResponse> getTrains() {
		return trains;
	}
	public void setTrains(List<TrainResponse> trains) {
		this.trains = trains;
	}
	public List<BusResponse> getBus() {
		return bus;
	}
	public void setBus(List<BusResponse> bus) {
		this.bus = bus;
	}
	public NPlanResponse getNplanResponse() {
		return nplanResponse;
	}
	public void setNplanResponse(NPlanResponse nplanResponse) {
		this.nplanResponse = nplanResponse;
	}
	public int getMatchPercent() {
		return matchPercent;
	}
	public void setMatchPercent(int matchPercent) {
		this.matchPercent = matchPercent;
	}
}
