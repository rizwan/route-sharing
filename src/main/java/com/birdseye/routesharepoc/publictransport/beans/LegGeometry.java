package com.birdseye.routesharepoc.publictransport.beans;

public class LegGeometry {
	private String points;
	private Integer length;
	
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	
}
