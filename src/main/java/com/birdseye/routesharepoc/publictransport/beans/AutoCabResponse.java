package com.birdseye.routesharepoc.publictransport.beans;

import com.birdseye.routesharepoc.beans.Coordinate;

public class AutoCabResponse {
	private Coordinate startPoint;
	private Coordinate endPoint;
	private String distance;
	private double matchPercent;
	public Coordinate getStartPoint() {
		return startPoint;
	}
	public void setStartPoint(Coordinate startPoint) {
		this.startPoint = startPoint;
	}
	public Coordinate getEndPoint() {
		return endPoint;
	}
	public void setEndPoint(Coordinate endPoint) {
		this.endPoint = endPoint;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public double getMatchPercent() {
		return matchPercent;
	}
	public void setMatchPercent(double matchPercent) {
		this.matchPercent = matchPercent;
	}
}
