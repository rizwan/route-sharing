package com.birdseye.routesharepoc.publictransport.beans;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * New planner response.
 * 
 * @author Ritesh
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class NPlanResponse{

	private List<Plan> plans;
	private Point source;
	private Point destination;

	public NPlanResponse() {
	}

	public List<Plan> getPlans() {
		return plans;
	}

	public void setPlans(List<Plan> plans) {
		this.plans = plans;
	}
	
	public Point getSource() {
		return source;
	}

	public void setSource(Point source) {
		this.source = source;
	}

	public Point getDestination() {
		return destination;
	}

	public void setDestination(Point destination) {
		this.destination = destination;
	}
	
//	@Override
//	public void writeToParcel(Parcel dest, int flags) {
//		// TODO Auto-generated method stub
//		dest.writeList(this.plans);
//	}
//
//	public static final Parcelable.Creator<NPlanResponse> CREATOR = new Parcelable.Creator<NPlanResponse>() {
//		public NPlanResponse createFromParcel(Parcel source) {
//			NPlanResponse mPlans = new NPlanResponse();
//
//			mPlans.plans = new ArrayList<Plan>();
//			source.readList(mPlans.plans, Plan.class.getClassLoader());
//
//			return mPlans;
//		}
//
//		public NPlanResponse[] newArray(int size) {
//			return new NPlanResponse[size];
//		}
//	};
//
//	@Override
//	public int describeContents() {
//		// TODO Auto-generated method stub
//		return 0;
//	}
}
