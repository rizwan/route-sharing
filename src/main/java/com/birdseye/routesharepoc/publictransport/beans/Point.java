package com.birdseye.routesharepoc.publictransport.beans;



public class Point  {
	private String point;
	private String name;
	private String lat;
	private String lon;
	private String stopid;
	
	public String getPoint() {
		return point;
	}

	public void setPoint(String point) {
		this.point = point;
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public Point() {
	}

	public String getStopid() {
		return stopid;
	}

	public void setStopid(String stopid) {
		this.stopid = stopid;
	}	
	
	public String toString(){
		return "\"" + this.name + "\"";
	}
}
