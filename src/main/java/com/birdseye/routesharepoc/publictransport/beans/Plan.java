package com.birdseye.routesharepoc.publictransport.beans;

import java.util.List;


public class Plan {

	private List<Plans> walkOnly;
	private List<Plans> autoOnly;
	private List<Plans> autoAndCab;

	public List<Plans> getWalkOnly() {
		return walkOnly;
	}

	public void setWalkOnly(List<Plans> walkOnly) {
		this.walkOnly = walkOnly;
	}

	public List<Plans> getAutoOnly() {
		return autoOnly;
	}

	public void setAutoOnly(List<Plans> autoOnly) {
		this.autoOnly = autoOnly;
	}
	
	public List<Plans> getAutoAndCab() {
		return autoAndCab;
	}

	public void setAutoAndCab(List<Plans> autoAndCab) {
		this.autoAndCab = autoAndCab;
	}

	/*@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		dest.writeList(this.walkOnly);
		dest.writeList(this.autoOnly);
		dest.writeList(this.autoAndCab);
	}

	// parcelable methods
	public static final Creator<Plan> CREATOR = new Creator<Plan>() {
		public Plan createFromParcel(Parcel source) {
			Plan mPlan = new Plan();

			mPlan.walkOnly = new ArrayList<Plans>();
			source.readList(mPlan.walkOnly, PlanWalkOnly.class.getClassLoader());

			mPlan.autoOnly = new ArrayList<Plans>();
			source.readList(mPlan.autoOnly, PlanAutoOnly.class.getClassLoader());

			mPlan.autoAndCab = new ArrayList<Plans>();
			source.readList(mPlan.autoAndCab, PlanAutoCab.class.getClassLoader());

			return mPlan;
		}

		public Plan[] newArray(int size) {
			return new Plan[size];
		}
	};*/

}
