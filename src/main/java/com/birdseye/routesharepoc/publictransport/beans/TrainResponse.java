package com.birdseye.routesharepoc.publictransport.beans;

public class TrainResponse {
	
	private String startStop;
	private String endStop;
	private String stops;
	private double matchPercent;
	public String getStops() {
		return stops;
	}

	public void setStops(String stops) {
		this.stops = stops;
	}

	public String getStartStop() {
		return startStop;
	}

	public void setStartStop(String startStop) {
		this.startStop = startStop;
	}

	public String getEndStop() {
		return endStop;
	}

	public void setEndStop(String endStop) {
		this.endStop = endStop;
	}

	public double getMatchPercent() {
		return matchPercent;
	}

	public void setMatchPercent(double matchPercent) {
		this.matchPercent = matchPercent;
	}
	
	
	
}
