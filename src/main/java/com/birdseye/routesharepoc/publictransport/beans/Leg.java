package com.birdseye.routesharepoc.publictransport.beans;

import java.util.List;



public class Leg  {
	private String mode;
	private String routeid;
	private String routeShortName;
	private String routeLongName;
	private String distance;
	private String duration;
	private String fare;
	private String agencyId;

	//private Agency agency;
	private Point startpoint;
	private Point endpoint;
	private List<String> walkinstructions;
	private String starttimestamp;
	private String endtimestamp;
	private List<Point> intermediatepoints;
	private List<Point> intermediatestops;
	//private List<Alert> alert;
	private LegGeometry legGeometry;
	private String selectedAgencyColour;
	private String agencyName;
	
	private String platformNumber;
	
	private double matchingPercent;
	

	public Leg() {
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getRouteid() {
		return routeid;
	}

	public void setRouteid(String routeid) {
		this.routeid = routeid;
	}

	public String getDistance() {
		return distance;
	}

	public void setDistance(String distance) {
		this.distance = distance;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFare() {
		return fare;
	}

	public void setFare(String fare) {
		this.fare = fare;
	}
/*
	public Agency getAgency() {
		return agency;
	}

	public void setAgency(Agency agency) {
		this.agency = agency;
	}*/

	public Point getStartpoint() {
		return startpoint;
	}

	public void setStartpoint(Point startpoint) {
		this.startpoint = startpoint;
	}

	public Point getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(Point endpoint) {
		this.endpoint = endpoint;
	}

	public List<String> getWalkinstructions() {
		return walkinstructions;
	}

	public void setWalkinstructions(List<String> walkinstructions) {
		this.walkinstructions = walkinstructions;
	}

	public List<Point> getIntermediatepoints() {
		return intermediatepoints;
	}

	public void setIntermediatepoints(List<Point> intermediatepoints) {
		this.intermediatepoints = intermediatepoints;
	}

	public List<Point> getIntermediatestops() {
		return intermediatestops;
	}

	public void setIntermediatestops(List<Point> intermediatestops) {
		this.intermediatestops = intermediatestops;
	}

	public String getStarttimestamp() {
		return starttimestamp;
	}

	public void setStarttimestamp(String starttimestamp) {
		this.starttimestamp = starttimestamp;
	}

	public String getEndtimestamp() {
		return endtimestamp;
	}

	public void setEndtimestamp(String endtimestamp) {
		this.endtimestamp = endtimestamp;
	}

	public String getRouteShortName() {
		return routeShortName;
	}

	public void setRouteShortName(String routeShortName) {
		this.routeShortName = routeShortName;
	}

	public String getRouteLongName() {
		return routeLongName;
	}

	public void setRouteLongName(String routeLongName) {
		this.routeLongName = routeLongName;
	}

	public LegGeometry getLegGeometry() {
		return legGeometry;
	}

	public void setLegGeometry(LegGeometry legGeometry) {
		this.legGeometry = legGeometry;
	}

	public String getSelectedAgencyColour() {
		return selectedAgencyColour;
	}

	public void setSelectedAgencyColour(String selectedAgencyColour) {
		this.selectedAgencyColour = selectedAgencyColour;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getAgencyName() {
		return agencyName;
	}

	public void setAgencyName(String agencyName) {
		this.agencyName = agencyName;
	}

	// parcelable methods
	/*public static final Creator<Leg> CREATOR = new Creator<Leg>() {
		public Leg createFromParcel(Parcel source) {
			return new Leg(source);
		}

		public Leg[] newArray(int size) {
			return new Leg[size];
		}
	};

	@Override
	public int describeContents() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {

		// first write simple fields
		dest.writeString(this.distance);
		dest.writeString(this.duration);
		dest.writeString(this.endtimestamp);
		dest.writeString(this.fare);
		dest.writeString(this.agencyId);
		dest.writeString(this.mode);
		dest.writeString(this.routeid);
		dest.writeString(this.routeShortName);
		dest.writeString(this.routeLongName);
		dest.writeString(this.starttimestamp);
		// then write parcels
		dest.writeValue(this.agency);
		dest.writeValue(this.startpoint);
		dest.writeValue(this.endpoint);
		// write list of parcels
		// TRFLog.d("Parcel crating",
		// "Writing parcel stop "+this.intermediatepoints.size());
		// TRFLog.d("Parcel crating",
		// "Writing parcel point"+this.intermediatestops.size());
		dest.writeStringList(walkinstructions);
		// if(this.intermediatepoints==null){
		// this.intermediatepoints= new ArrayList<Point>();
		// Point p1= new Point();
		// p1.setName("hi");
		// this.intermediatepoints.add(p1);
		// }
		dest.writeList(this.intermediatepoints);
		// if(this.intermediatestops==null){
		// this.intermediatestops= new ArrayList<Point>();
		// }
		dest.writeList(this.intermediatestops);
		dest.writeValue(this.legGeometry);
		dest.writeString(this.selectedAgencyColour);
		dest.writeString(this.agencyName);

		// TODO Auto-generated method stub

	}

	private Leg(Parcel in) {
		// //first write simple fields
		this.distance = in.readString();
		this.duration = in.readString();
		this.endtimestamp = in.readString();
		this.fare = in.readString();
		this.agencyId = in.readString();
		this.mode = in.readString();
		this.routeid = in.readString();
		this.routeShortName = in.readString();
		this.routeLongName = in.readString();
		this.starttimestamp = in.readString();
		// then read parcels
		this.agency = (Agency) in.readValue(Agency.class.getClassLoader());
		this.startpoint = (Point) in.readValue(Point.class.getClassLoader());
		this.endpoint = (Point) in.readValue(Point.class.getClassLoader());
		// read list of parcels
		this.walkinstructions = new ArrayList<String>();
		in.readStringList(this.walkinstructions);
		this.intermediatepoints = new ArrayList<Point>();
		in.readList(this.intermediatepoints, Point.class.getClassLoader());
		this.intermediatestops = new ArrayList<Point>();
		in.readList(this.intermediatestops, Point.class.getClassLoader());
		this.legGeometry = (LegGeometry) in.readValue(LegGeometry.class
				.getClassLoader());
		this.selectedAgencyColour = in.readString();
		this.agencyName = in.readString();
	}*/

	/*public List<Alert> getAlert() {
		return alert;
	}

	public void setAlert(List<Alert> alert) {
		this.alert = alert;
	}*/

	public String getPlatformNumber() {
		return platformNumber;
	}

	public void setPlatformNumber(String platformNumber) {
		this.platformNumber = platformNumber;
	}

	@Override
	public String toString() {
		return "Leg [mode=" + mode + ", routeid=" + routeid
				+ ", routeShortName=" + routeShortName + ", routeLongName="
				+ routeLongName + ", distance=" + distance + ", duration="
				//+ duration + ", fare=" + fare + ", agency=" + agency
				+ ", startpoint=" + startpoint + ", endpoint=" + endpoint
				+ ", walkinstructions=" + walkinstructions
				+ ", starttimestamp=" + starttimestamp + ", endtimestamp="
				+ endtimestamp + ", intermediatepoints=" + intermediatepoints
				+ ", intermediatestops=" + intermediatestops + ", alert="
				//+ alert + "]"
				;
	}

	public double getMatchingPercent() {
		return matchingPercent;
	}

	public void setMatchingPercent(double matchingPercent) {
		this.matchingPercent = matchingPercent;
	}

}
