package com.birdseye.routesharepoc.publictransport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.publictransport.beans.Leg;
import com.birdseye.routesharepoc.publictransport.beans.Point;
import com.birdseye.routesharepoc.publictransport.dao.PlannerDAO;

@Service("busPlannerAlgo")
public class BusPlannerAlgo implements PlannerAlgo{

	@Autowired
	PlannerDAO plannerDAO;
	
	@Override
	public int process(Leg leg) {
		Point startPoint = leg.getStartpoint();
		Point endPoint = leg.getEndpoint();
		int routeNo = Integer.parseInt(leg.getRouteid());
		
		
		int id = plannerDAO.getBusPlan(routeNo, startPoint.getName(), endPoint.getName());
		return id;
	}
}
