package com.birdseye.routesharepoc.publictransport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.beans.MatchingEdge;
import com.birdseye.routesharepoc.bo.PolylineEncoder;
import com.birdseye.routesharepoc.bo.RouteBO;
import com.birdseye.routesharepoc.publictransport.beans.Leg;
import com.birdseye.routesharepoc.publictransport.beans.Point;
import com.birdseye.routesharepoc.publictransport.dao.PlannerDAO;

@Service("autoAndCabPlannerAlgo")
public class AutoAndCabPlannerAlgo implements PlannerAlgo {

	@Autowired
	RouteBO routeBO;
	
	@Autowired 
	PlannerDAO plannerDao;
	
	@Override
	public int process(Leg leg) {
		
		String points = leg.getLegGeometry().getPoints();
		List<Coordinate> listCoordinate = PolylineEncoder.decode(points, 0.00001);
	
		List<MatchingEdge> listmatchingEdge = routeBO.getRoute("", "", listCoordinate.get(0), listCoordinate.get(listCoordinate.size()-1), 80);
		MatchingEdge matchingEdge = listmatchingEdge.get(0);
		leg.setMatchingPercent(matchingEdge.getMatchingPercent());
		int routeId = matchingEdge.getRouteId();
		if(matchingEdge.isNew()){
			plannerDao.addCab(0, routeId);
			routeId = -1;
		}
		return routeId;
	}

}
