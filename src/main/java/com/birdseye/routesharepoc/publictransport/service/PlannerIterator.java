package com.birdseye.routesharepoc.publictransport.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.bo.PolylineEncoder;
import com.birdseye.routesharepoc.dao.RouteDAO;
import com.birdseye.routesharepoc.publictransport.beans.AutoCabResponse;
import com.birdseye.routesharepoc.publictransport.beans.BusResponse;
import com.birdseye.routesharepoc.publictransport.beans.Leg;
import com.birdseye.routesharepoc.publictransport.beans.NPlanResponse;
import com.birdseye.routesharepoc.publictransport.beans.Plan;
import com.birdseye.routesharepoc.publictransport.beans.Plans;
import com.birdseye.routesharepoc.publictransport.beans.Point;
import com.birdseye.routesharepoc.publictransport.beans.SharePTResponse;
import com.birdseye.routesharepoc.publictransport.beans.TrainResponse;
import com.birdseye.routesharepoc.publictransport.dao.PlannerDAO;

@Service("plannerIterator")
@Scope("prototype")
public class PlannerIterator {
	private Coordinate start;
	private Coordinate end;

	@Autowired
	AlgoFactory algoFactory;

	@Autowired
	RouteDAO routeDAO;

	@Autowired
	PlannerDAO plannerDAO;


	NPlanResponse nplanResponse;

	public SharePTResponse sharePTResponse = null;
	List<Double> listMatchPercent = null;

	HashMap<String, List<Integer>> hash = new HashMap<String, List<Integer>>();

	public boolean doTheBusiness(Coordinate start, Coordinate end){
		listMatchPercent = new ArrayList<Double>();
		this.start =start; 
		this.end = end;
		boolean matchPlan = true;
		nplanResponse = getPlanner();
		 sharePTResponse = new SharePTResponse();
		for(Plan plan : nplanResponse.getPlans()){
			List<Plans> lstPlans = plan.getAutoAndCab();
			if(lstPlans != null && lstPlans.size() < 0)
				return true;
			Plans plans = lstPlans.get(0);
			for(Leg leg : plans.getLegs()){
				PlannerAlgo algo = algoFactory.getAlgo(leg.getMode());
				int id = processMode(algo, leg);
				if(id == 0){
					sharePTResponse.setNplanResponse(nplanResponse);
					return false;
				}
				else if(id > 0){
					if(leg.getMode().equals("BUS")){
						BusResponse busResponse = new BusResponse();
						busResponse.setStartStop(leg.getStartpoint().toString());
						busResponse.setEndStop(leg.getEndpoint().toString());
						busResponse.setRouteNo(leg.getRouteid());
						busResponse.setMatchPercent(100.0);
						sharePTResponse.getBus().add(busResponse);
						listMatchPercent.add(100.0);
					}
					if(leg.getMode().equals("RAIL")){
						TrainResponse trainRes = new TrainResponse();
						trainRes.setStartStop(leg.getStartpoint().toString());
						trainRes.setEndStop(leg.getEndpoint().toString());
						trainRes.setStops(leg.getIntermediatestops().toString());
						trainRes.setMatchPercent(100.0);
						sharePTResponse.getTrains().add(trainRes);
						listMatchPercent.add(100.0);
					}
					if(leg.getMode().equals("CAB") || leg.getMode().equals( "AUTO")){
						AutoCabResponse autoCabRes = new AutoCabResponse();
						List<Coordinate> lstCoordinate = PolylineEncoder.decode(leg.getLegGeometry().getPoints(),0.00001);
						autoCabRes.setStartPoint(lstCoordinate.get(0));
						autoCabRes.setEndPoint(lstCoordinate.get(lstCoordinate.size()-1));
						autoCabRes.setDistance(leg.getDistance());
						autoCabRes.setMatchPercent(leg.getMatchingPercent());
						sharePTResponse.getAutoCab().add(autoCabRes);
						
						listMatchPercent.add(leg.getMatchingPercent());
					}

					String mode = leg.getMode().equals("AUTO") ? "CAB" : leg.getMode();
					if(hash.containsKey(mode)){
						hash.get(mode).add(id);
					}
					else{
						hash.put(mode, new ArrayList<Integer>() {

							private static final long serialVersionUID = 1L;

							public ArrayList<Integer> addOne(int val){
								this.add(val);
								return this;
							}
						}.addOne(id));
					}
				}
			}
			matchPlan = isSharedRoute(hash);
			if(matchPlan){
				double sum = 0;
				for(Double val : listMatchPercent){
					sum += val;
				}
				double average = sum /listMatchPercent.size();
				sharePTResponse.setMatchPercent((int)average);
			}
		}
		return matchPlan;
	}

	public void addRoute(Coordinate sourcePoint, Coordinate destinationPoint){

		//NPlanResponse nplanResponse = getPlanner();
		int route_id = plannerDAO.addRoute(sourcePoint, destinationPoint);
		for(Plan plan : nplanResponse.getPlans()){
			List<Plans> lstPlans = plan.getAutoAndCab();
			Plans plans = lstPlans.get(0);
			if(lstPlans != null && lstPlans.size() < 0)
				return;
			for(Leg leg : plans.getLegs()){
				PlannerAlgo algo = algoFactory.getAlgo(leg.getMode());
				int id = processMode(algo, leg);
				if(id == 0){
					if(leg.getMode().equals("BUS")){
						plannerDAO.addBusPlan(route_id, leg.getRouteid(), leg.getStartpoint().getName(), leg.getEndpoint().getName());
					}
					if(leg.getMode().equals("RAIL")){
						List<Point> stopList = leg.getIntermediatestops();
						String stops =   stopList.toString().substring(1, stopList.toString().length()-1);
						plannerDAO.addTrainPlan(route_id, stops);
					}
					if(leg.getMode().equals("CAB") || leg.getMode().equals( "AUTO")){
						//plannerDAO.addCab(route_id, 0);
					}
				}
				else{
					if(leg.getMode().equals("BUS")){
						plannerDAO.updateBusMapping(route_id, id);
					}
					if(leg.getMode().equals("RAIL")){
						plannerDAO.updateTrainMapping(route_id, id);
					}
					if(leg.getMode().equals("CAB") || leg.getMode().equals( "AUTO")){
						if(id == -1){
							System.out.println("id = -1");
							id = processMode(algo, leg);
							System.out.println("new Id " + id);
						}
						plannerDAO.updateCabMapping(route_id, id);
					}
				}
			}

		}
	}

	private int processMode(PlannerAlgo algo, Leg leg){
		int processed = algo.process(leg);
		return processed;
	}

	private NPlanResponse getPlanner(){
		PlannerImp plannerImp = new PlannerImp();
		String response = plannerImp.callAPI(start, end);
		return plannerImp.parseAPIResponse(response);
	}

	private boolean isSharedRoute(HashMap<String, List<Integer>> modes){

		List<Integer> cabIds = null;
		List<Integer> trainIds = null;
		List<Integer> busIds = null;

		List<Integer> route_bus_id = new ArrayList<Integer>();
		List<Integer> route_train_id = new ArrayList<Integer>();;
		List<Integer> route_cab_id = new ArrayList<Integer>();;

		List<Integer> common = null;
		if(modes.containsKey("BUS")){
			busIds = modes.get("BUS");
			if(busIds != null)
				route_bus_id = plannerDAO.getRoutesForBusId(busIds);
		}
		if(modes.containsKey("RAIL")){

			trainIds = modes.get("RAIL");
			if(trainIds != null)
				route_train_id = plannerDAO.getRoutesForTrainId(trainIds);

			if(!CollectionUtils.isEmpty(route_bus_id)){
				common = (List<Integer>)CollectionUtils.intersection(route_bus_id, route_train_id);
			}
		}
		if(modes.containsKey("CAB")){
			cabIds = modes.get("CAB");
			if(cabIds != null){
				route_cab_id = plannerDAO.getRoutesForCabId(cabIds);

				if(!CollectionUtils.isEmpty(common)){
					common = (List<Integer>)CollectionUtils.intersection(common, route_cab_id);
				}
				else if(!CollectionUtils.isEmpty(route_bus_id)){
					common = (List<Integer>)CollectionUtils.intersection(route_bus_id, route_cab_id);

				}
				else if(!CollectionUtils.isEmpty(route_train_id)){
					common = (List<Integer>)CollectionUtils.intersection(route_train_id, route_cab_id);
				}
				else{
					common = route_cab_id;
				}
			}
		}

		return !CollectionUtils.isEmpty(common);
		

		/*		//List<Integer> common = null;
		if(cabIds != null && busIds != null){
			common = new ArrayList<Integer>(CollectionUtils.intersection(cabIds, busIds));
			cabDone = true; 
			busDone = true;
		}
		if(cabIds != null && trainIds != null){
			common = new ArrayList<Integer>(CollectionUtils.intersection(cabIds, trainIds));
			cabDone = true; trainDone = true;
		}
		if(trainIds != null && busIds != null){
			common = new ArrayList<Integer>(CollectionUtils.intersection(busIds, trainIds));
			trainDone = true; busDone = true;
		}

		if(common != null){
			if(cabDone && busDone){
				if(trainIds != null)
					common = new ArrayList<Integer>(CollectionUtils.intersection(common, trainIds));
			}

			if (cabDone && trainDone){
				if(busIds != null)
					common = new ArrayList<Integer>(CollectionUtils.intersection(common, busIds));
			}

			if(trainDone && busDone){
				if(cabIds != null)
					common = new ArrayList<Integer>(CollectionUtils.intersection(common, cabIds));
			}
		}

		if(common != null && common.size() > 0)
			return true;

		return false;*/
	}
}
