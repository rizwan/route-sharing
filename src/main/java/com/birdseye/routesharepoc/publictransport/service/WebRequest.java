package com.birdseye.routesharepoc.publictransport.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

public class WebRequest {
	public enum Method {
		GET,
		POST
	}

	String url = "";
	String params = "";
	String contentType = "";
	Method method = null;
	HashMap<String, String> requestHeader;
	HashMap<String, String> requestParameters;
	public WebRequest(String url, String params){
		this.url = url;
		this.params = params;
	}

	public String make(Method method, String contentType) throws Exception{
		return processRequest();
	}

	/**
	 * Make web request and get response
	 * @param request method GET or POST
	 * @return response in string
	 * @throws Exception
	 */
	public String make(Method method) throws Exception{
		this.method = method;
		return processRequest();
	}

	private String processRequest() throws Exception{
		String response = "";
		if(method.equals(Method.GET)){
			this.url += "?" + params;
			response = doGet();
		}
		else if(method.equals(Method.POST)){
			response = doPost();
		}
		return response;
	}
	
	private void prepareParams(){
		if(requestParameters == null)
			return;
		Set<Entry<String,String>> set = requestParameters.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		StringBuilder strBld = new StringBuilder();
		while(iterator.hasNext()){
			Entry<String, String> entry  = iterator.next();
			strBld.append(entry.getKey() + "=" + entry.getValue() + "&");
			//con.setRequestProperty(entry.getKey(), entry.getValue());
		}
		strBld.deleteCharAt(strBld.length());
		this.params = strBld.toString();
	}

	private String doGet() throws IOException{	


		URL obj = new URL(this.url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		prepareParams();
		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		//con.setRequestProperty("User-Agent", USER_AGENT);

		//System.out.println("\nSending 'GET' request to URL : " + url);
		//System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);

		}
		return response.toString();
	}

	private String doPost() throws IOException{
		URL obj = new URL(this.url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		prepareParams();
		//add request header
		con.setRequestMethod("POST");
		//con.setRequestProperty("User-Agent", USER_AGENT);
		Set<Entry<String,String>> set = requestHeader.entrySet();
		Iterator<Entry<String, String>> iterator = set.iterator();
		while(iterator.hasNext()){
			Entry<String, String> entry  = iterator.next();
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}
		String urlParameters = this.params;

		// Send post request
		con.setDoInput(true);
		con.setRequestProperty("Content-Length", Integer.toString(urlParameters.getBytes().length));
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.write(urlParameters.getBytes("UTF-8"));
		wr.flush();
		wr.close();

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		return response.toString();
	}

	public void addHeader(String key, String value){
		if(null == requestHeader){
			requestHeader = new HashMap<String, String>();
		}
		requestHeader.put(key, value);
	}

	public void addParameters(String key, String value){
		if(null == requestParameters){
			requestParameters = new HashMap<String, String>();
		}
		requestParameters.put(key, value);
	}
}
