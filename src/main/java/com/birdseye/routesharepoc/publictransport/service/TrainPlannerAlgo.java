package com.birdseye.routesharepoc.publictransport.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.publictransport.beans.Leg;
import com.birdseye.routesharepoc.publictransport.beans.Point;
import com.birdseye.routesharepoc.publictransport.dao.PlannerDAO;

@Service ("trainPlannerAlgo")
public class TrainPlannerAlgo implements PlannerAlgo{

	@Autowired
	PlannerDAO plannerDAO;
	
	@Override
	public int process(Leg leg) {
		// TODO Auto-generated method stub
		List<Point> stopList = leg.getIntermediatestops();
		String stops = stopList.toString().substring(1, stopList.toString().length()-1);
		int id = plannerDAO.getTrainPlan(stops);
		return id;
	}
}
