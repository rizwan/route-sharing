package com.birdseye.routesharepoc.publictransport.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.publictransport.beans.NPlanResponse;
import com.birdseye.routesharepoc.publictransport.beans.Request;
import com.birdseye.routesharepoc.publictransport.service.WebRequest.Method;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;


public class PlannerImp implements Planner {

	private static final String REQUEST_URL = "http://tomcat.ridlr.in:8080/traffline-api/planner";


	@Override
	public String callAPI(Coordinate start, Coordinate end) {
		
		String response = "" ;
		
		String jsonRequest = "";
		
		List<List<Double>> points = new ArrayList<List<Double>>();
		List<Double> start_point = new ArrayList<Double>();
		List<Double> end_point = new ArrayList<Double>();
		
		Request request = getRequestObject();
		
		start_point.add(start.getLatitude());
		start_point.add(start.getLongitude());
		
		
		end_point.add(end.getLatitude());
		end_point.add(end.getLongitude());
		
		points.add(start_point);
		points.add(end_point);
		
		request.setPoints(points);
		
		request.setDepartureTime(new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()));
		
		try {
			jsonRequest = new ObjectMapper().writeValueAsString(request);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		WebRequest webRequest = new WebRequest(REQUEST_URL, jsonRequest);
		
		webRequest.addHeader("contentType", "application/json");
		try {
			response = webRequest.make(Method.POST);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public NPlanResponse parseAPIResponse(String response) {
		NPlanResponse nPlanResponse =null;
		try {
			nPlanResponse = new ObjectMapper().readValue(response, NPlanResponse.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return nPlanResponse;
	}

	private Request getRequestObject(){
		Request request = null;
		String strRequest = "{\"arrivalTime\": null, "+
				"\"cityId\": \"1\"," + 
				"\"departureTime\": \"\","+ 
				"\"destinationPlace\": \"Mulund West, Mumbai, Maharashtra, India\","+ 
				"\"destinationPlaceId\": null,"+ 
				"\"deviceId\": \"bd364d654e42c80b\","+ 
				"\"localTransport\": [\"AUTO_AND_CAB\"]," +
				"\"mode\": [\"2\",\"3\",\"0\"]," + 
				"\"points\": [[ 19.0390214, 72.86189519999994  ],[ 19.1725542, 72.94253700000002 ] ],"+ 
				"\"preference\": \"LT\", " +
				"\"resultPerLocalTransport\": \"3\", " +
				"\"sourcePlace\": \"Sion, Mumbai, Maharashtra, India\", " +
				"\"sourcePlaceId\": null,\"userId\": \"-9\"}";
		try {
			request = new ObjectMapper().readValue(strRequest, Request.class);
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return request;
	}

}
