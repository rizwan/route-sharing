package com.birdseye.routesharepoc.publictransport.service;

import com.birdseye.routesharepoc.publictransport.beans.Leg;

public interface PlannerAlgo {
	public int process(Leg algo);
}
