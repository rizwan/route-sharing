package com.birdseye.routesharepoc.publictransport.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.birdseye.routesharepoc.publictransport.beans.Leg;

@Service("algoFactory")
public class AlgoFactory implements PlannerAlgo{
	
	@Autowired
	AutoAndCabPlannerAlgo autoAndCabPlannerAlgo;
	
	@Autowired
	BusPlannerAlgo busPlannerAlgo;
	
	@Autowired
	TrainPlannerAlgo trainPlannerAlgo;
	
	public PlannerAlgo getAlgo(String mode){
		PlannerAlgo algo = null;
		switch (mode.toUpperCase()){
		case "CAB":
		case "AUTO":
			algo = autoAndCabPlannerAlgo;
			break;
		case "BUS":
			algo = busPlannerAlgo;
			break;
		case "RAIL":
			algo = trainPlannerAlgo;
			break;
		default:
			return null;
		}
		return algo;
	}

	@Override
	public int process(Leg leg) {
		// TODO Auto-generated method stub
		return 0;
	}
}
