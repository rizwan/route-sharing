package com.birdseye.routesharepoc.publictransport.service;

import com.birdseye.routesharepoc.beans.Coordinate;
import com.birdseye.routesharepoc.publictransport.beans.NPlanResponse;

public interface Planner {
	String callAPI(Coordinate start, Coordinate end);
	NPlanResponse parseAPIResponse(String response);
}
