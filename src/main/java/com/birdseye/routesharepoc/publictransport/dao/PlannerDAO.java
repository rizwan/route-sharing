package com.birdseye.routesharepoc.publictransport.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import com.birdseye.routesharepoc.beans.Coordinate;

@Repository("plannerDAO")
public class PlannerDAO {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate();
		this.jdbcTemplate.setDataSource(dataSource);
	}


	public int getBusPlan(Integer routeNo, String startStop, String endStop){
		int id = 0;
		try{String sql = "select bus_id from bus where route_no = '" + routeNo + "' AND  start_stop = "
				+ "'" + startStop + "' and end_stop = '" + endStop + "'";
		id = jdbcTemplate.queryForObject(sql, Integer.class);
		return id;
		}catch(EmptyResultDataAccessException ex){
			return id;
		}
	}

	public int getTrainPlan(String stops){
		int id = 0;
		try{
			String sql = "select train_id from (select train_id from train where stop_name = ('" + 
					stops + "')) as t group by train_id"
					+ " ";
			id = jdbcTemplate.queryForObject(sql, Integer.class);
		}catch(EmptyResultDataAccessException ex){
			//
		}

		return id;
	}

	public int addCab(final int routeId, final int walkId){
		final String sql = "insert into cab (route_id) values (?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

				PreparedStatement ps = con.prepareStatement(sql, new String[]{"train_id"});
				ps.setInt(1, (walkId));

				return ps;
			}
		}, keyHolder);

		final int cabId = keyHolder.getKey().intValue();
		if(routeId == 0)
			return cabId;
		updateCabMapping(routeId, cabId);
		return cabId;
	}

	public int addTrainPlan(final int routeId, final String stops){
		final String sql = "INSERT INTO train (stop_name) values (?)";

		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				// TODO Auto-generated method stub

				PreparedStatement ps = con.prepareStatement(sql, new String[]{"train_id"});
				ps.setString(1, (stops));

				return ps;
			}
		}, keyHolder);

		final int trainId = keyHolder.getKey().intValue();
		updateTrainMapping(routeId, trainId);
		return trainId;
	}

	public int addBusPlan(final int routeId, final String routeNo, final String startStop, final String endStop){
		final String sql = "INSERT INTO bus (route_no, start_stop, end_stop) values (?,?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {

				PreparedStatement ps = con.prepareStatement(sql);
				ps.setString(1, (routeNo));
				ps.setString(2, startStop);
				ps.setString(3, endStop);
				return ps;
			}
		}, keyHolder);

		final int busId = keyHolder.getKey().intValue();
		updateBusMapping(routeId, busId);
		return busId;
	}

	public int updateBusMapping(final int routeId, final int busId){
		String updateSql = "insert into route_bus(route_id, bus_id) values (?,?)";
		return jdbcTemplate.update(updateSql, new PreparedStatementSetter(){
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, routeId);
				ps.setInt(2, busId);
			}
		});
	}

	public int updateTrainMapping(final int routeId, final int trainId){
		String updateSql = "insert into route_train(route_id, train_id) values (?,?)";
		return jdbcTemplate.update(updateSql, new PreparedStatementSetter(){
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, routeId);
				ps.setInt(2, trainId);
			}
		});
	}

	public int updateCabMapping(final int routeId, final int cabId){
		String updateSql = "insert into route_cab (route_id, cab_id) values (?,?)";
		return jdbcTemplate.update(updateSql, new PreparedStatementSetter(){
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setInt(1, routeId);
				ps.setInt(2, cabId);
			}
		});
	}


	public int addRoute (final Coordinate start, final Coordinate end){
		final String sql = "INSERT INTO route_pt (source, destination) values (?,?)";
		KeyHolder keyHolder = new GeneratedKeyHolder();
		jdbcTemplate.update(new PreparedStatementCreator() {

			@Override
			public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
				// TODO Auto-generated method stub

				PreparedStatement ps = con.prepareStatement(sql,new String[]{"route_id"});
				ps.setString(1, String.valueOf(start.toString()));
				ps.setString(2, end.toString());
				return ps;
			}
		}, keyHolder);
		int routeId =  keyHolder.getKey().intValue();
		//String updateSql = "Insert into route_otp_mapping (route_id) values ("+ routeId +")";
		//jdbcTemplate.update(updateSql);
		return routeId;
	}
	
	public List<Integer> getRoutesForCabId(List<Integer> cabIds){

		List<Integer> listIds = null;
		try{
			String cabId = "";
			for(int i = 0; i < cabIds.size(); i++)
			{			
				cabId += cabIds.get(i) + ",";
			}
			cabId = cabId.substring(0, cabId.length() -1);
			String sql = "select distinct route_id from route_cab where cab_id in (" + cabId + ")";
			listIds = jdbcTemplate.queryForList(sql, Integer.class);
			return listIds;
		}
		catch(EmptyResultDataAccessException ex){
			return listIds;
		}
	}
	
	public List<Integer> getRoutesForBusId(List<Integer> busIds){

		List<Integer> listIds = null;
		String busId = "";
		for(int i = 0; i < busIds.size(); i++)
		{			
			busId += busIds.get(i) + ",";
		}
		busId = busId.substring(0, busId.length() -1);
		try{
			String sql = "select distinct route_id from route_bus where bus_id in (" + busId + ")";
			listIds = jdbcTemplate.queryForList(sql, Integer.class);
			return listIds;
		}
		catch(EmptyResultDataAccessException ex){
			return listIds;
		}
	}
	
	public List<Integer> getRoutesForTrainId(List<Integer> trainIds){

		List<Integer> listIds = null;
		String trainId = "";
		for(int i = 0; i < trainIds.size(); i++)
		{			
			trainId += trainIds.get(i) + ",";
		}
		trainId = trainId.substring(0, trainId.length() -1);
		try{
			String sql = "select distinct route_id from route_train where train_id in (" + trainId + ")";
			listIds = jdbcTemplate.queryForList(sql, Integer.class);
			return listIds;
		}
		catch(EmptyResultDataAccessException ex){
			return listIds;
		}
	}
}
