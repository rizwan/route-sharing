CREATE DATABASE  IF NOT EXISTS `gts` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gts`;
DROP TABLE IF EXISTS `edge`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `edge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `edge_id` int(11) DEFAULT NULL,
  `edge_val` int(11) DEFAULT NULL,
  `edge_weight` double DEFAULT NULL,
  PRIMARY KEY (`id`)
)

INSERT INTO `edge` VALUES (1,1,1,NULL),(2,1,2,NULL),(3,1,3,NULL),(4,1,4,NULL),(5,1,5,NULL),(6,2,2,NULL),(7,2,3,NULL),(8,2,4,NULL),(9,2,5,NULL),(10,2,2,NULL),(11,2,3,NULL),(12,2,4,NULL),(13,2,5,NULL);

CREATE TABLE `edge_path` (
  `edge_id` int(11) DEFAULT NULL,
  `path` text
)

DROP TABLE IF EXISTS `route`;
CREATE TABLE `route` (
  `route_id` int(11) DEFAULT NULL,
  `source_place_id` varchar(128) DEFAULT NULL,
  `destination_place_id` varchar(128) DEFAULT NULL,
  `source_coordinates` varchar(50) DEFAULT NULL,
  `destination_coordinates` varchar(50) DEFAULT NULL
)

CREATE TABLE `route_edge_mapping` (
  `id` int(11) NOT NULL,
  `route_id` int(11) NOT NULL,
  `edge_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
)
